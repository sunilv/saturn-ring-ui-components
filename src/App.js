import React from "react";

import Button from "./components/Button";
import TextInput from "./components/TextInput";
import LogoNodejs from "react-ionicons/lib/LogoNodejs";
function App() {
  return (
    <div
      className="App"
      style={{
        margin: "10px"
      }}
    >
      <h1>SaturnQ React Components</h1>
      <h3>Buttons</h3>
      <div
        className="App"
        style={{
          height: "200px",
          width: "300px",
          display: "flex",
          justifyContent: "space-around",
          flexDirection: "column"
        }}
      >
        <Button label="Get Started" size="small" />
        <Button
          icon={<LogoNodejs />}
          label="Get Started"
          size="medium"
          background="blue"
        />
        <Button
          size="large"
          icon={<LogoNodejs />}
          label="Get Started"
          iconPosition="right"
          background="purple"
        />
      </div>
      <h3>Text Input</h3>
      <div
        className="App"
        style={{
          height: "200px",
          width: "300px",
          display: "flex",
          justifyContent: "space-around",
          flexDirection: "column",
          backgroundColor: "#800000",
          padding: 10
        }}
      >
        <TextInput theme="dark" />
        <TextInput theme="light" />
      </div>
      <h3>Nav Bar</h3>
      <div
        className="App"
        style={{
          height: "200px",
          width: "300px",
          display: "flex",
          justifyContent: "space-around",
          flexDirection: "column",
          backgroundColor: "#800000",
          padding: 10
        }}
      >
        <TextInput theme="dark" />
        <TextInput theme="light" />
      </div>
    </div>
  );
}

export default App;
