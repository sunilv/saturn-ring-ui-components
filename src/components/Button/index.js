import React, { useEffect, useContext, useState } from "react";
import PropTypes from "prop-types";
import styled, { keyframes, css } from "styled-components";
import IosArrowForward from "react-ionicons/lib/IosArrowForward";

var tinycolor = require("tinycolor2");

const HOVER_COLOR_STRENGTH = 5;
const TEXT_COLOR_STRENGTH = 40;

let textColor = "#ccc";
let hoverColor = "#ccc";

const buttonHover = keyframes`
  0% {
    opacity: 0.5;
  }
  100% {
    opacity: 1;
  }
`;

const StyledButton = styled.button`
  background-color: ${props => props.background};
  border-radius: 13px;
  cursor: pointer;
  color: ${props => props.color};
  padding: 12px 20px;
  text-decoration: none;
  box-shadow: none;
  border: 0px;
  text-transform: uppercase;
  overflow: hidden;
  ${props =>
    props.width &&
    props.height &&
    css`
        width: ${props.width};
        height: ${props.height};
    }`}
  ${props =>
    props.isHovered &&
    css`
      &:hover{
        animation: ${buttonHover} 0.4s forwards;
      }
    }`}
`;

const IconWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: center;
`;

const getReverseColor = (color, reveseStrength) => {
  const backgroundColor = tinycolor(color);
  if (backgroundColor.isDark()) {
    return backgroundColor.brighten(reveseStrength).toString();
  }
  return backgroundColor.darken(reveseStrength).toString();
};

const renderIcon = icon => {
  return React.cloneElement(icon, { color: textColor, size: 10 });
};
const getButtonChildren = (label, icon, iconPosition = "left") => {
  if (label && icon) {
    const iconElement = renderIcon(icon);
    return (
      <IconWrapper>
        {iconPosition === "left" ? iconElement : null}
        {label}
        {iconPosition === "right" ? iconElement : null}
      </IconWrapper>
    );
  }
  return label;
};

/**
 * Button component
 *
 * @param   {string}    className styled-components className for custom styling
 * @returns {component}           React component
 */

const Button = ({ size, background, label, icon, iconPosition }) => {
  textColor = getReverseColor(background, TEXT_COLOR_STRENGTH);
  hoverColor = getReverseColor(background, HOVER_COLOR_STRENGTH);
  const [isHovered, setIsHovered] = useState(false);
  const buttonChildren = getButtonChildren(label, icon, iconPosition);
  return (
    <>
      <StyledButton
        background={background}
        color={textColor}
        hoverColor={hoverColor}
        isHovered={isHovered}
        onMouseOver={() => setIsHovered(true)}
        onMouseOut={() => setIsHovered(false)}
      >
        {buttonChildren}
      </StyledButton>
    </>
  );
};

Button.propTypes = {
  size: PropTypes.oneOf(["small", "medium", "large"]),
  background: PropTypes.string,
  label: PropTypes.string,
  icon: PropTypes.elementType,
  iconPosition: PropTypes.oneOf(["left", "right"])
};

Button.defaultProps = {
  size: "large",
  background: "yellow",
  label: "primary"
};

export default Button;
